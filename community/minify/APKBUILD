# Maintainer: Wesley van Tilburg <justwesley@protonmail.com>
pkgname=minify
pkgver=2.20.6
pkgrel=0
pkgdesc="Minifier CLI for HTML, CSS, JS, JSON, SVG and XML"
url="https://github.com/tdewolff/minify"
arch="all"
options="net"
license="MIT"
makedepends="go bash"
subpackages="$pkgname-bash-completion"
source="$pkgname-$pkgver.tar.gz::https://github.com/tdewolff/minify/archive/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	mkdir build
	go build -o build ./cmd/minify
}

check() {
	go test ./...
}

package() {
	install -Dm755 ./build/minify -t "$pkgdir"/usr/bin
	install -Dm644 ./cmd/minify/bash_completion "$pkgdir"/usr/share/bash-completion/completions/minify
}

sha512sums="
7baabccf7dae0e878466182ff0b489c5f0dbd0b5dfb533d974ca98ee06592401d7d8e94932662644149cfa36bb9eeb05afe5e0fc6391ecd3ba2b25dbbc874246  minify-2.20.6.tar.gz
"
